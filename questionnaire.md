# Questionnaire

These questions are very important to us. Explain yourself as much as you need! We don't mind reading :)
Also, you can attach any diagrams if you see that it's useful.

1. What do you like most about TS and JS? What do you like the least?

   > When I started as a developer, I decided to start with web development because of the large number of possibilities or uses that it has. And mainly because of JS. I think that JS is a very powerful and versatile language, since it allows you to develop everything you need for an application (FrontEnd, BackEnd, microservices) and even mobile applications. I think the possibilities in this language are endless. In addition, the concept of asynchronous tasks (thanks to the event loop), seems super powerful to me. In short, what I like most about javascript is its versatility. What I like least is that it is not a strongly typed language and this can be a problem when working in a team and achieving a clean and well-structured code. And this is what I like the most about TS, which complements and enhances JS even more, but especially for developers, thanks to its strong typing and all the features (Object programming, among others) that it adds. In this way, an object-oriented and strongly typed programming language is achieved, together with the asynchrony and versatility of js.

2. If we are using an event based system and a handler that is triggered by an event fails. How would you handle that error?

   > Depending on the event relevancy, what I’d suggest is to implement a retry strategy, where if the event wasn’t acknowledged it would have to be resend into the system at some point in the future. The “some point into the future” would be done X seconds after the retry, and increasing with every retry. The goal here is not to choke the system with endless retries if multiple calls were to fail.

3. How would you feel if tomorrow we switch to another programming language, cloud provider, framework, whatever? What aspects would you consider for making that decision? 

   > I think that when making a decision such as changing a language, cloud provider, framework, among others, it is important to assess the impact that this will have on the product and the team. The aspects that I would consider would be: the balance between the benefits and the necessary work, if the team knows this technology or if some training is needed, the time necessary for its implementation and the impact it will have on the client (Delays in development, possible errors ), understand the reason for the change (change in suppliers, performance improvements, possible deprecation) and above all, and above all, the commitment/opinion of the team. I think it is important to involve all the teams that may be affected in order to find the best implementation (QA, devs, architects, PM, etc).
   For my part, I would feel enthusiastic and predisposed to discuss the best implementation. In the end, if the change is to improve the product, it is best to discuss it, evaluate it and carry it out together

4. Which cloud service would you use to build a scalable web service (e.g REST API)? Which components would you use? And how would they interact?

   > I think that AWS and Google cloud are the best providers to build a scalable service, since they provide infinite tools for its development and work in a modular way, allowing a service to scale automatically at runtime. Choosing AWS, I would use the EC2 service as a cluster, since it has the ability to auto scale resources as needed and a load balancer, S3 for persistent file storage, RDS as a relational database and Route 53 for DNS. Through Route 53, DNS zones can be defined so that clients have access to the service via HTTPS, which is hosted on EC2, which has a load balancer and the possibility of auto scaling. This cluster would communicate internally to the RDS.

5. Tell us about the last blog post, book you’ve read, websites you follow, or podcast you heard, related to programming or management. Did you learn something new?

   > I usually read stack overflow, the bible for every developer, when I have an error or problem that I can't find a solution to. Then, I also read quite a few medium blogs and official sites of the frameworks/libraries/technologies that I use, to know when I should update them and what the new features are. For example, the last thing I read is about the update of node js to version 17 in medium, to know the changes and the opinions about it. Also recently, I have started to investigate how to improve/monitor the quality of a sw and I find myself implementing/testing sonarqube and its implementation within CI/CD, as well as the different tests that can be done (unit test, integration test, component test, e2e, etc)
