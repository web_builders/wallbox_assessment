# Package JSON
    - I tried to run all the scripts from package.json, but I receive an error (At least in Windows and VSC's terminal): ""'node" is not recognized as an internal or external command". Due to that, I decided to install cross-env library and edit package json (It should work in all OS)
    - I had the same issue trying to run "npm run start:device -i dABCD". Even sending -- -i, --i, and other options, but parameter dABCD is not received and the device is setted to d0001 by default. Due to that, I added more scripts in package.json to be able to test it.

# Assumptions
    - I assumed that the charger starts before the device (start server, start charger, start device).