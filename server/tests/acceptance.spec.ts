import { ChargerMock, createChargerMock } from "../../mocks/charger/charger.mock";
import { createDeviceMock, DeviceMock } from "../../mocks/device/device.mock";

import { expect } from "chai";
import { stub } from 'sinon';

let device: DeviceMock;
let charger: ChargerMock;

const deviceId = "d0001";
const deviceUrl = "ws://localhost:3200/devices";

const chargerId = "c0001";
const chargerUrl = "ws://localhost:3100/chargers";

const updateChargeStatusIndicatorFnSpy = stub();

before(async () => {
  // Create mocks
  device = createDeviceMock({
    id: deviceId,
    url: deviceUrl,
    updateChargeStatusIndicatorFn: updateChargeStatusIndicatorFnSpy
  });
  charger = createChargerMock({
    id: chargerId,
    url: chargerUrl
  });

  // start device and charger
  await charger.start();
  await device.start();
});

beforeEach(() => {});

describe("Device gets Statuses from charger", () => {
  it("Device gets Status charging from charger", (done) => {
    charger.setSoc(10);
    updateChargeStatusIndicatorFnSpy.callsFake((status) => {
      expect(status).to.be.eq('charging');
      done();
    });
  });

  it("Device gets Status charging80 from charger", (done) => {
    charger.setSoc(80);
    updateChargeStatusIndicatorFnSpy.callsFake((status) => {
      expect(status).to.be.eq('charging80');
      done();
    });
  });

  it("Device gets Status charged from charger", (done) => {
    charger.setSoc(100);
    updateChargeStatusIndicatorFnSpy.callsFake((status) => {
      expect(status).to.be.eq('charged');
      done();
    });
  });
});
