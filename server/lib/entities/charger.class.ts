import { ChargerEvent } from "../types/charger";
import { Device } from "./device.class";
import { DeviceEvent } from "./deviceEvent.class";

export class Charger{
    id: string; // Charger Id
    soc: number; // State of charge (Porcentaje) 
    device?: Device | null; // Device Object

    /**
     * Init Charger
     * 
     * @param id Charger id
     * @param soc State of charge (Porcentaje)
     * @param device Device Object
     */
    constructor(id: string, soc: number = 0, device?: Device){
        this.id = id;
        this.soc = soc;
        this.device = device;
    }
    
    /**
     * When an event is received, it should update SOC and send event to the device
     * 
     * @param event Charger Event object
     */
    onEvent(event: ChargerEvent){
        // Update SOC
        this.soc = event.data.soc;
        // send event to the device
        if(this.device?.ws){
            this.device.ws.send(new DeviceEvent('ChargingStatus', this.soc).toString());
        }
    }
}