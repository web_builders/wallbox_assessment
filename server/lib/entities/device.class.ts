import WebSocket from "ws";
import { ChargeStatus } from "../types/charger";

export class Device{
    id: String; // Device ID
    status?: ChargeStatus; // State of charge (String)
    ws?: WebSocket; // Web socket connection

    /**
     * Init Device
     * 
     * @param id Device Id
     * @param status State of Charge
     * @param ws Web Socket
     */
    constructor(id: String, status?: ChargeStatus, ws?: WebSocket){
        this.id = id;
        this.status = status;
        this.ws = ws;
    }
}