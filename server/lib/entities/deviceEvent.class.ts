import { ChargeStatus } from "../types/charger"
import { DeviceEvents } from "../types/device"

export class DeviceEvent{
    event: DeviceEvents;
    data:{
        status: ChargeStatus;
    };

    /**
     * Create an Device Event
     * 
     * @param event Device event
     * @param soc State of Charge
     */
    constructor(event: DeviceEvents, soc: number){
        this.event = event;
        this.data = {
            status: 'charged'
        };

        if(soc < 80){
            this.data.status = 'charging';
        }else if (soc < 100){
            this.data.status = 'charging80';
        }
    }

    /**
     * Convert event to string
     * 
     * @returns Object as String
     */
    toString(): string{
        return JSON.stringify(this);
    }
}