import WebSocket from "ws";
import { ChargerService } from "./services/charger.service";
import { DeviceService } from "./services/device.service";

export function startServer(): Promise<WebSocket.Server[]> {
  // init servers
  return Promise.all([new ChargerService().start(), new DeviceService().start()]);
}

export function stopServer(servers: WebSocket.Server[] | null): Promise<any> {
  if (Array.isArray(servers)) {
    return Promise.all(servers.map((server) => server.close()));
  } else {
    return Promise.resolve();
  }
}
