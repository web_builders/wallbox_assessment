export type ChargeStatus = 'charging' | 'charging80' | 'charged';
export type ChargerEvents = 'StateOfCharge';

export type ChargerEvent = {
    event: ChargerEvents;
    data:{
        soc: number;
    }
};