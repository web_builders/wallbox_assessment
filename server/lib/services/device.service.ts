import { IncomingMessage } from "http";
import WebSocket from "ws";
import { Device } from "../entities/device.class";
import { ChargerDeviceMapSingleton } from "./chargerDeviceMap";
import { CommonServer } from "./commonServer.service";

export class DeviceService extends CommonServer{
    chargerDeviceMapInstance: ChargerDeviceMapSingleton;

    /**
     * Init Service
     * 
     */
     constructor(){
        super('devices', 3200);
        this.chargerDeviceMapInstance = ChargerDeviceMapSingleton.getInstance();
    }

    /**
     * On Connection
     * 
     * @param ws Web socket
     * @param req Incoming message
     */
     onConnection(ws: WebSocket, req: IncomingMessage){
        // get information from URL
        const urlAsArray = req.url?.split('/');
        // get elements from array
        const [,type, id] = urlAsArray || [];

        // Check event
        if(type === this.type && id){
            console.log(`${this.type} connection to: `, req.url);
            this.chargerDeviceMapInstance.assignDeviceToCharger(new Device(id, 'charging', ws))
        }

        // Close connection
        ws.on("close", () => {
            this.chargerDeviceMapInstance.removeDevice(id);
            console.log(`${this.type} connection closed`, req.url);
        });
    }
}