import { IncomingMessage } from "http";
import WebSocket from "ws";

export class CommonServer{
    type: string; // Type of server
    port: number; // Server port

    /**
     * Init common server
     * 
     * @param type type of server
     * @param port port
     */
    constructor(type: string, port: number){
        this.type = type;
        this.port = port;
    }

    /**
     * Start server
     * 
     * @returns Web socket server
     */
    start(): Promise<WebSocket.Server>{
        return new Promise((resolve) => {
            const wss = new WebSocket.Server({ port: this.port });
        
            wss.on("connection", (ws: WebSocket, req: IncomingMessage) => this.onConnection(ws, req));
        
            wss.on("listening", () => {
              resolve(wss);
            });
          });
    }

    /**
     * On Connection
     * 
     * @param ws Web socket
     * @param req Incoming message
     */
    onConnection(ws: WebSocket, req: IncomingMessage){
        // get information from URL
        const urlAsArray = req.url?.split('/');
        // get elements from array
        const [,type, id] = urlAsArray || [];
        // Check event
        if(type === this.type && id){
            console.log(`${this.type} connection to: `, req.url);
            ws.on("message", (message: string) => this.onMessage(id, message));
        }

        // Close connection
        ws.on("close", () => {
            console.log(`${this.type} connection closed`, req.url);
        });
    }

    /**
     * On message
     * 
     * @param id device id
     * @param message Incomming message
     */
    onMessage(id: string, message: string){
        console.log(message);
        // Override function to manage messages
    }
}