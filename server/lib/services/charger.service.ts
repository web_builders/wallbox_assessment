import { Charger } from "../entities/charger.class";
import { CommonServer } from "./commonServer.service";
import { ChargerEvent } from '../types/charger';
import { ChargerDeviceMapSingleton } from "./chargerDeviceMap";
import { IncomingMessage } from "http";
import WebSocket from "ws";

export class ChargerService extends CommonServer{
    chargers: Charger[]; // List of chargers
    chargerDeviceMapInstance: ChargerDeviceMapSingleton;

    /**
     * Init Service
     * 
     */
    constructor(){
        super('chargers', 3100);
        this.chargerDeviceMapInstance = ChargerDeviceMapSingleton.getInstance();
        // observable for chargers
        this.chargerDeviceMapInstance.chargers$.subscribe((chargers: Charger[]) => {
            this.chargers = chargers;
        })
        
    }

    /**
     * On Connection
     * 
     * @param ws Web socket
     * @param req Incoming message
     */
     onConnection(ws: WebSocket, req: IncomingMessage){
        // get information from URL
        const urlAsArray = req.url?.split('/');
        // get elements from array
        const [,type, id] = urlAsArray || [];
        // Check event
        if(type === this.type && id){
            console.log(`${this.type} connection to: `, req.url);
            // add charger
            this.chargerDeviceMapInstance.addCharger(id);
            // send message
            ws.on("message", (message: string) => this.onMessage(id, message));
        }

        // Close connection
        ws.on("close", () => {
            this.chargerDeviceMapInstance.removeCharger(id);
            console.log(`${this.type} connection closed`, req.url);
        });
    }

    /**
     * On message
     * 
     * @param id charger id
     * @param message Incomming message
     */
     onMessage(id: string, message: string){
        // parse message
        const parsedMessage: ChargerEvent = JSON.parse(message);
        // get charger
        const charger: Charger | undefined = this.chargers.find(c => c.id === id);

        // If charger is not available, return
        if(!charger) return;

        // validate message
        if (parsedMessage.event === "StateOfCharge"){
            // send event
            charger.onEvent(parsedMessage);
        }
    }
}