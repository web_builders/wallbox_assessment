import { BehaviorSubject } from "rxjs";
import { Charger } from "../entities/charger.class";
import { Device } from "../entities/device.class";
import { chargerDeviceMap } from "../map/chargesDeviceMap";

export class ChargerDeviceMapSingleton{
    chargers: Charger[] = []; // Array of charges
    chargers$: BehaviorSubject<Charger[]>; // Observable of chargers
    private static instance: ChargerDeviceMapSingleton;

    /**
     * Init Service
     * 
     * @param chargersIds Array of charger ids
     */
    constructor(chargersIds: string[] = []){
        this.chargers = chargersIds.map(id => new Charger(id));
        this.chargers$ = new BehaviorSubject(this.chargers);
    }

    /**
     * The static method that controls the access to the singleton instance.
     *
     * 
     *  @param chargersIds Array of charger ids
     */
     public static getInstance(chargersIds: string[] = []): ChargerDeviceMapSingleton {
        if (!ChargerDeviceMapSingleton.instance) {
            ChargerDeviceMapSingleton.instance = new ChargerDeviceMapSingleton(chargersIds);
        }

        return ChargerDeviceMapSingleton.instance;
    }

    /**
     * Assign device to charger (We are considering that the numerical id is the same for both) c0001 - d0001
     * 
     * @param device device
     */
    assignDeviceToCharger(device: Device){
        // get num id
        const chargedId = [...chargerDeviceMap.entries()].filter(({1: dId}) => dId === device.id).map(([k]) => k)[0];

        // find charger with same numId
        const chargerIdx = this.chargers.findIndex(c => c.id === chargedId);

        if(chargerIdx >= 0){
            // Assign device
            this.chargers[chargerIdx].device = device;
            // emit event
            this.chargers$.next(this.chargers);
        }
    }

    /**
     * Add a new charger and emit event
     * 
     * @param id charger id
     */
    addCharger(id: string){
        this.chargers.push(new Charger(id));
        this.chargers$.next(this.chargers);
    }

    /**
     * Remove a charger and emit event
     * 
     * @param id charger id
     */
     removeCharger(id: string){
        this.chargers = this.chargers.filter(c => c.id == id);
        this.chargers$.next(this.chargers);
    }

    /**
     * Remove a device and emit event
     * 
     * @param id device id
     */
    removeDevice(id: string){
        // get num id
        const chargedId = [...chargerDeviceMap.entries()].filter(({1: dId}) => dId === id).map(([k]) => k)[0];

        // find charger with same numId
        const chargerIdx = this.chargers.findIndex(c => c.id === chargedId);

        // remove charger
        if(chargerIdx >= 0){
            // Assign device
            this.chargers[chargerIdx].device = null;
            // emit event
            this.chargers$.next(this.chargers);
        }
    }
}